const request = require('request')
const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')

if (!process.argv[2]) {
    console.log('No location was provided.')
} else {
    geocode(process.argv[2], (error, { latitude, longitude, location}) => {
        if (error) {
            console.log('Error:', error)
        } else {
            forecast(latitude, longitude, (error, forecastData) => {
                if (error) {
                    console.log('Error:', error)
                } else {
                    console.log(location)
                    console.log(forecastData)
                }
            })
        }
    
    })
}

