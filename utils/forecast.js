const request = require('request')

const forecast = (latitude, longitude, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=a4f3ac9bd493d3a391fb4217e6e4e945&query=' + latitude + ',' + longitude + '&units=f'
    request({url, json: true}, (error, { body }) => {
        if (error) {
            callback('Unable to connect to weather services!')
        } else if (body.error) {
            callback('Unable to find location.')
        } else {
            weather = body.current.weather_descriptions[0] + ". It is currently " + body.current.temperature + " degrees out. It feels like " + body.current.feelslike + " degrees out."
            callback(undefined, weather)
        }
    })
}

module.exports = forecast